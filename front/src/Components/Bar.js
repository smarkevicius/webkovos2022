import React, { useState, useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

import { UserContext } from "../App";

const Bar = (props) => {


  const style = {
    color : "white",
    

  } ;


  return (

    <nav className="navbar navbar-dark navbar-expand-lg bg-dark">
      <div className="container-fluid">
      <div className="navbar-brand ps-4 col-4">Projektas
      <Link to="/"
              style={{ color: "white" }}
              className="text-decoration-none">
              <button className="btn"  style={style}>
                <h4>Marsas</h4>
              </button>
            </Link>
            </div>



        <ul className="navbar-nav me-auto mb-lg-0">
   
         <li className="nav-item">
            <Link to="/"
              style={{ color: "white" }}
              className="text-decoration-none">
              <button className="btn btn-secondary m-2">
                Pradinis puslapis
              </button>
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/photos"}
              style={{ color: "white" }}
              className="text-decoration-none">
              <button className="btn btn-secondary m-2">
                Išsaugotos nuotraukos
              </button>
            </Link>
          </li>
        </ul>


      </div>

    </nav>



  );

}

export default Bar;