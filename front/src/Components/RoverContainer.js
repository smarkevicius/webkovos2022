import React from 'react'
import axios from 'axios'

import Card from './Card'
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';


const RoverContainer = () => {

    const rovers = ["curiosity", "opportunity", "spirit"];

    const apiKey = "&api_key=cSbEoaSh3nAfkaKnpcdrQQ9qPGqxXCRjO9VA9yhR";
    const nasaApi = "https://api.nasa.gov/mars-photos/api/v1/manifests/";

    
    const cameras = {
        curiosity: ["FHAZ", "RHAZ", "MAST", "CHEMCAM", "MAHLI", "MARDI", "NAVCAM"],
        opportunity: ["FHAZ", "RHAZ", "NAVCAM", "PANCAM", "MINITES"],
        spirit: ["FHAZ", "RHAZ", "NAVCAM", "PANCAM", "MINITES"]
    };
    const selfies = {
        curiosity: "https://cdn.theatlantic.com/thumbor/iXVmc1hCyGR6lTgkZ9eNrpPh6YU=/1200x843/media/img/photo/2013/08/one-year-on-mars-the-curiosity-rover/m01_RTR3DOV2/original.jpg",
        opportunity: "https://images.indianexpress.com/2018/10/mars-rover-2020-copy.jpg",
        spirit: "https://cbsnews1.cbsistatic.com/hub/i/r/2010/07/30/838d183c-a642-11e2-a3f0-029118418759/thumbnail/1200x630/939032e040d138d35db8f4651bd37c70/spirit.jpg"
    };

    const [state, setState] = useState({
        curiosity: { date: "a" },
        opportunity: { date: "b" },
        spirit: { date: "c" }
    });
    const [curiosity, setCuriosity] = useState({ date: null });
    const [opportunity, setOpportunity] = useState({ date: null });
    const [spirit, setSpirit] = useState({ date: null });


    const [loadState, setLoadState] = useState({ loaded: false });

    const axios = require('axios');


    useEffect(() => {

        const load = async () => {
            try {

                let ndata = null;
                rovers.forEach(rover => {

                         axios.get(nasaApi + rover + "?" + apiKey).then(resp => {

                        ndata = resp.data;
                        const date = ndata.photo_manifest.landing_date ;
                        switch (rover) {
                            case "curiosity": setCuriosity({ date: ndata.photo_manifest.landing_date }); break;
                            case "opportunity": setOpportunity({ date: ndata.photo_manifest.landing_date }); break;
                            case "spirit": setSpirit({ date: ndata.photo_manifest.landing_date }); break;
                        }


                    });
                }



                );
                




            } catch (err) {
                console.log(err);
            }



        }

        /// console.log("use effect");
        setTimeout(load, 150);


    }, []);

    useEffect(() => {

      //  console.log(curiosity);
       // console.log(opportunity);
       // console.log(spirit);


    }, [curiosity,opportunity,spirit]);


    if (curiosity.date ===null &&
        opportunity.date ===null &&
        spirit.date ===null) {
        return (<div>Kraunasi</div>)
    }
    else
        return (

            <div className="container ">
                <h4>Marsaeigiai</h4>

                <div className="row">

                    <Card key={"curiosity"}
                        name={"curiosity"}
                        date={"2012-08-05"}
                        cameras={cameras["curiosity"]}
                        image={selfies["curiosity"]}
                    />

                    <Card key={"opportunity"}
                        name={"opportunity"}
                        date={"2004-01-24"}
                        cameras={cameras["opportunity"]}
                        image={selfies["opportunity"]}
                    />


                    <Card key={"spirit"}
                        name={"spirit"}
                        date={"2004-01-03"}
                        cameras={cameras["spirit"]}
                        image={selfies["spirit"]}
                    />


                </div>

            </div>

        );

}


export default RoverContainer;