import React from 'react';
import { Link } from 'react-router-dom';

const DetailsButton = (props) => {


    onclick = () => {

    }

    if (props.viewMode) {
        return (
            <Link to={`/${props.name}`} style={{ color: "white" }} className="text-decoration-none">
                <button className="btn btn-secondary"
                    onClick={onclick} >
                    Nuotraukos
                </button>
            </Link>);
    }
    else return (<> </>);


}
export default DetailsButton;