import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import Modal from 'react-modal';
import axios from 'axios';
import Thumbnails from './Thumbnails';

const RoverPhotosContainter = () => {

    let { id } = useParams();

    let subtitle="";
    const cameras = {
        curiosity: ["FHAZ", "RHAZ", "MAST", "CHEMCAM", "MAHLI", "MARDI", "NAVCAM"],
        opportunity: ["FHAZ", "RHAZ", "NAVCAM", "PANCAM", "MINITES"],
        spirit: ["FHAZ", "RHAZ", "NAVCAM", "PANCAM", "MINITES"]
    };


    const apiKey = "&api_key=cSbEoaSh3nAfkaKnpcdrQQ9qPGqxXCRjO9VA9yhR";
    const nasaApi = "https://api.nasa.gov/mars-photos/api/v1/rovers/";

    const [state,setState] = useState({cameras : null});
    const [activeCamera,setActiveCamera] = useState({camera : null});
    const [photos,setPhotos] = useState({photoArray : null});
    const [cameraWasSelected,setSelected] = useState(false);
    const [modalIsOpen, setIsOpen] = React.useState(false);

    Modal.setAppElement(document.getElementById('root'));

    const axios = require('axios');

    const load = async (camera) => {
        try {


            axios.get(nasaApi + id + "/photos?sol=1000&camera=" + camera +apiKey).then(resp => {
                
                console.log(resp.data);
                if(resp.data.photos.length === 0) {
                    alert("Fotografiju nera");
                }
                else {

                    setPhotos({photoArray : resp.data.photos});
                }


            });


                  
    } catch (err) {
        console.log(err);
    }

}
    const openModal = () => {


        if(photos.photoArray !== null) {
            console.log("y");
        }



     








        setIsOpen(true);









      }

      const  closeModal = () => {
        setIsOpen(false);
      }
    
      function afterOpenModal() {
        // references are now sync'd and can be accessed.
        subtitle.style.color = '#f00';
      }
     
useEffect(() => {
    if(id === "curiosity") {
        setState({cameras:cameras.curiosity});
    }else 
    if(id === "opportunity") {
        setState({cameras:cameras.opportunity});
    }else
    if(id === "spirit") {
        setState({cameras:cameras.spirit});
    }

},[]);
     
useEffect( () => {

    if(cameraWasSelected)
        load(activeCamera.camera);



},[activeCamera,cameraWasSelected]);



const onClick = (e) => {
    console.log(e.target.textContent)
    setSelected(true);
    setActiveCamera({camera : e.target.textContent});

    openModal();

}

        if(state.cameras !== null)

        return (<div>

            {id}

            <p>kameros</p>

            <ul>
                {state.cameras.map(cam =>  
                     <li key={cam} > 
              
                        <div className="btn btn-5"
                          onClick={e=>onClick(e)}>
                            {cam}
                         </div>
                        <Modal
                              isOpen={modalIsOpen}
                            onAfterOpen={afterOpenModal}
                            onRequestClose={closeModal}
                            style={{color:"black"}}
                            contentLabel=""
                            > 
                               <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Nuotraukos</h2>
                             <div className ="btn btn-secondary" onClick={closeModal}>close</div>
                             <div className="d-grid"> 
                            
                                  <Thumbnails src={photos}></Thumbnails>
                             </div>
                             </Modal>
                     </li>

                    
                )} 
                
            </ul>

        </div>);
 


   else return (<div> ner</div>);

}

export default RoverPhotosContainter;