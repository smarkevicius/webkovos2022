import React, { Component } from "react";
import { Link } from "react-router-dom";
import { useContext } from "react/cjs/react.development";
import DetailsButton from "./DetailsButton";
 

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: this.props.key,
       name :this.props.name,
       date : this.props.date,
       cameras : this.props.cameras,
      buttonTitle: "Details",
      viewMode: this.props.viewMode,
      imageUrl: this.props.image
    };
  }



  cardClick = () => {

  }

  render() {
    return (
      <div className="card m-2" style={{ width: 18 + "rem" }}>
        <span className="row justify-content-center"> {this.state.name} </span>
        <img
          style = {{minHeight : "200px"}}
          src={this.state.imageUrl}
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title">{this.state.cardTitle}</h5>
          <div className="card-text">
            
            <p>Nusileido {this.state.date} </p>
            <p>Kameros </p>
            <ul> {this.state.cameras.map(camera =>  <li key={camera}>{camera}</li> )}</ul>
          </div>


          <DetailsButton viewMode={true}

            name={this.state.name} />

         


        </div>
      </div>
    );
  }
}
export default Card;