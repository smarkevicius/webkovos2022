import React, { useState } from "react";
import './App.css';
import Bar from './Components/Bar'
import {
    BrowserRouter,
    Switch,
    Route,
} from "react-router-dom";
import Footer from "./Components/Footer";
import RoverContainter from "./Components/RoverContainer";
 import NotFound from "./Components/NotFound";
 import RoverPhotosContainter from "./Components/RoverPhotosContainer";
 import SavedPhotosContainer from "./Components/SavedPhotosContainer";
 
export const AppContext = React.createContext({});
 


const App = () => {



    const [appState, setAppState] = useState({ state1: "", state2: [] });


    return (  
        <div className="container">
            <AppContext.Provider value={{ appState: appState, setAppState }}>
                <BrowserRouter basename={process.env.PUBLIC_URL}>

                    <Bar />

                    <Switch>

                        <Route exact path="/">
                            <RoverContainter />
                        </Route>
                   
                        <Route path="/photos">
                            <SavedPhotosContainer />
                        </Route>

                        <Route path="/:id">
                         <RoverPhotosContainter />  
                        </Route>

                        <Route component={NotFound} />
                    </Switch>

                    <Footer />


                </BrowserRouter>
            </AppContext.Provider >
        </div>

    );

}
export default App;


