package it.akademija.Reservation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.akademija.Apartment.Apartment;
import it.akademija.Apartment.ApartmentService;

@Service
public class ReservationService {
	
	@Autowired
	private ReservationDAO reservationDAO;
	
	@Autowired
	private ApartmentService apartmentService;
	
	@Transactional(readOnly = true)  
	public List<Reservation> getAllReservations() {
		
		return reservationDAO.findAll();
	}
	
	@Transactional
	public void deleteReservationById(Long id) {
		reservationDAO.deleteById(id);
	}
	
	
	@Transactional
	public Reservation createReservation(Long id, ReservationDTO dto) {
		
		Reservation r = new Reservation();
		
		Apartment a = apartmentService.findApartmentById(id);
	
		if(a!= null) {
			r.setReservedAt();
			r.setApartment(a);
		}
		r.setCreatedAt();
		r.setEmail(dto.getEmail());
		r.setFirstName(dto.getFirstName());
		r.setLastName(dto.getLastName());
		r.setMessage(dto.getMessage());
		r.setPhone(dto.getPhone());
		r.setUpdatedAt();
	
		
		
		return reservationDAO.saveAndFlush(r);
		
	}
	
	@Transactional
	public void deleteReservation(Reservation r) {
		
		reservationDAO.delete(r);
	}
 
}
