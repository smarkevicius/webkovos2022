package it.akademija.Reservation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "Reservation controller")
@RequestMapping(value = "/api/v1/reservations")
public class ReservationController {
	
	@Autowired
	private ReservationService reservationService;
	
	
	@ApiOperation(value = "Get all reservations") 
	@RequestMapping(method = RequestMethod.GET)
	public List<Reservation> getAllReservations() {
		return reservationService.getAllReservations();
	}

	

}
