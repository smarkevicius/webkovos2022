package it.akademija.Reservation;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationDAO extends JpaRepository<Reservation, Long>{

	public List<Reservation> findAll() ;
	
	public void deleteReservationByApartmentId(Long id) ;
	
}
