package it.akademija;

public enum Status {
	
	AVAILABLE, RESERVED, SOLD;

}
