package it.akademija.Apartment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ApartmentDAO extends JpaRepository<Apartment, Long>{

	List<Apartment> findAll();
	
	

}
