package it.akademija.Apartment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

 import it.akademija.Status;
import it.akademija.Reservation.Reservation;

@Entity
public class Apartment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	 
	@Column
	private String name;
	
	@Column
	private String locaiton;
	
	@Column
	private int floor;
	
	@Column
	private int bedrooms;
	
	@Column
	private int carSpaces;
	
	@Column
	private int bathrooms;
	
	@Column
	private int area;
	
	@Column
	private BigDecimal price;
	
	@Column
	private Status status;

	@Column
	private LocalDateTime dateSellFrom;
	
	@Column
	private LocalDateTime dateSellTo;
	
	@Column
	private LocalDateTime createdAt;
	
	@Column
	private LocalDateTime updatedAt;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="reservation_id", referencedColumnName = "id")
	private Reservation reservation;
	
	
	public Apartment() {
		
	}
 
	public Reservation getReservation() {
		return this.reservation;
	}
	
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
	private void setUpdatedAt() {
		this.updatedAt = LocalDateTime.now();
	}
	
	public LocalDateTime getUpdatedAt() {
		return this.updatedAt;
	}
	
	public LocalDateTime getCreatedAt() {
		return this.createdAt;
	}
	
	public void setCreatedAt() {
		this.createdAt = LocalDateTime.now();
	}
	
	public Long getId () {
		return this.id;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocaiton() {
		return locaiton;
	}

	public void setLocaiton(String locaiton) {
		this.locaiton = locaiton;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public int getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(int bedrooms) {
		this.bedrooms = bedrooms;
	}

	public int getCarSpaces() {
		return carSpaces;
	}

	public void setCarSpaces(int carSpaces) {
		this.carSpaces = carSpaces;
	}

	public int getBathrooms() {
		return bathrooms;
	}

	public void setBathrooms(int bathrooms) {
		this.bathrooms = bathrooms;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public LocalDateTime getDateSellFrom() {
		return dateSellFrom;
	}

	public void setDateSellFrom(LocalDateTime dateSellFrom) {
		this.dateSellFrom = dateSellFrom;
	}

	public LocalDateTime getDateSellTo() {
		return dateSellTo;
	}

	public void setDateSellTo(LocalDateTime dateSellTo) {
		this.dateSellTo = dateSellTo;
	}

 
	
	
	
	
}
