package it.akademija.Apartment;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.akademija.Reservation.Reservation;
import it.akademija.Reservation.ReservationDTO;
import it.akademija.Reservation.ReservationService;

@RestController
@Api(tags = "Apartment and reservation controller")
@RequestMapping("/api/v1/apartments")
public class ApartmentController {
	
	@Autowired
	private ApartmentService apartmentService;
	
	@Autowired
	private ReservationService reservationService;
	
	@ApiOperation(value = "Get all apartments")
	@RequestMapping(method = RequestMethod.GET)
	public List<Apartment> getApartments() throws FileNotFoundException, IOException, CsvException {
		
		
	 List<Apartment> list = apartmentService.getAllApartments();
		if (list.size() > 0 )
			 return list;
		else
		{
			try (CSVReader reader = new CSVReader(new FileReader("src/main/resources/apartments.csv"))) {
			      List<String[]> r = reader.readAll();
			      r.forEach(x -> {
			    	  ApartmentDTO dto = new ApartmentDTO();
			    	  String[] vals = Arrays.toString(x).split(",");
			    	  dto.setName(vals[0]);
			    	  dto.setLocation(vals[1]);
			    	  dto.setBedrooms(Integer.parseInt(vals[2]));
			    	  apartmentService.createApartment(dto);
			      	}
			      );
			  }
			
		}
		return null;
			
	}
	
	@ApiOperation(value = "Create new apartment")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED) 
	public void createCenter(@ApiParam(value="apartment data")
				@RequestBody ApartmentDTO dto) {
		
	 Apartment a = 	apartmentService.createApartment(dto);
		System.out.println("created "+ a.getId());
		
		
	}
	
	@ApiOperation(value = "Delete apartment by id")
	@RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteApartment(@ApiParam(value="apartment id")
		@PathVariable Long id ) {
		
		System.out.println("Deleted: "+ id);
		apartmentService.deleteApartmentById(id);
	}
	
	
	
	@ApiOperation(value = "create reservation for apartment by id" )
	@RequestMapping(method = RequestMethod.POST, path= "/{id}/reserve")
	@ResponseStatus(HttpStatus.CREATED)
	public void reserveApartment(@ApiParam(value ="apartment id")
		@PathVariable Long id,
		@ApiParam(value = "reservation details")
		@RequestBody ReservationDTO dto) {
		
		
			reservationService.createReservation(id, dto);
			
		
	}

	
	
		@ApiOperation(value = "Delete reservation") 
		@RequestMapping(method = RequestMethod.DELETE, path = "/{id}/deletereservation")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public void deleteReservation(@ApiParam(value = "Apt id")
			@PathVariable Long id) {
			
			Apartment a = apartmentService.findApartmentById(id);
			if (a != null && a.getReservation() != null) {
				Reservation r = a.getReservation();
			 
				  reservationService.deleteReservation(r);
				  a.setReservation(null);
			}
		}
			

}
