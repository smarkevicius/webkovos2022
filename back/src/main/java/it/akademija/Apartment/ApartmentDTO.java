package it.akademija.Apartment;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import it.akademija.Status;

public class ApartmentDTO {
	
	 
	private String name;
	private String locaiton;
	private int floor;

	private int bedrooms;
	private int carSpaces;
	private int bathrooms;
	private int area;
	private BigDecimal price;

	private Status status;
	private LocalDateTime dateSellFrom;
	private LocalDateTime dateSellTo;

	
	public ApartmentDTO() {
		
	}

	

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLocaiton() {
		return locaiton;
	}


	public void setLocaiton(String locaiton) {
		this.locaiton = locaiton;
	}


	public int getFloor() {
		return floor;
	}


	public void setFloor(int floor) {
		this.floor = floor;
	}


	public int getBedrooms() {
		return bedrooms;
	}


	public void setBedrooms(int bedrooms) {
		this.bedrooms = bedrooms;
	}


	public int getCarSpaces() {
		return carSpaces;
	}


	public void setCarSpaces(int carSpaces) {
		this.carSpaces = carSpaces;
	}


	public int getBathrooms() {
		return bathrooms;
	}


	public void setBathrooms(int bathrooms) {
		this.bathrooms = bathrooms;
	}


	public int getArea() {
		return area;
	}


	public void setArea(int area) {
		this.area = area;
	}


	public BigDecimal getPrice() {
		return price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public LocalDateTime getDateSellFrom() {
		return dateSellFrom;
	}


	public void setDateSellFrom(LocalDateTime dateSellFrom) {
		this.dateSellFrom = dateSellFrom;
	}


	public LocalDateTime getDateSellTo() {
		return dateSellTo;
	}


	public void setDateSellTo(LocalDateTime dateSellTo) {
		this.dateSellTo = dateSellTo;
	}



	public void setLocation(String string) {
		this.locaiton = string;
		
	}
	
	
}
