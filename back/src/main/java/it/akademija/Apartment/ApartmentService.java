package it.akademija.Apartment;

import java.util.List;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ApartmentService {
	
	@Autowired
	private ApartmentDAO apartmentDAO;
	
	
	@Transactional(readOnly = true)
	public Apartment findApartmentById(Long id) {
		
		return apartmentDAO.findById(id).orElse(null);
		 
	}
	
	
	@Transactional(readOnly = true)  
	public List<Apartment> getAllApartments() {
		return apartmentDAO.findAll();
	}
	
	@Transactional
	public void deleteApartmentById(Long id) {
		 apartmentDAO.deleteById(id);
	}
	
	@Transactional
	public Apartment createApartment(ApartmentDTO dto) {
		
		Apartment a = new Apartment();
		a.setCreatedAt();
	 	a.setArea(dto.getArea());
	 	a.setBathrooms(dto.getBathrooms());
	 	a.setBedrooms(dto.getBedrooms());
	 	a.setCarSpaces(dto.getCarSpaces());
	 	a.setDateSellFrom(dto.getDateSellFrom());
	 	a.setDateSellTo(dto.getDateSellTo());
	 	a.setFloor(dto.getFloor());
	 	a.setLocaiton(dto.getLocaiton());
	 	a.setPrice(dto.getPrice());
	 	a.setStatus(dto.getStatus());
	 	a.setReservation(null);
	 	a.setName(dto.getName());
	 	
	 	return apartmentDAO.saveAndFlush(a);
	 	
	 	
	 	
	}
	
	
	

}
